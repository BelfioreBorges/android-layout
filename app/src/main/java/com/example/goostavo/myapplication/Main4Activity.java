package com.example.goostavo.myapplication;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class Main4Activity extends AppCompatActivity {

    Button button_green;
    Button button_yellow;
    Button button_blue;
    Button button_red1;
    Button button_black;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);


        button_green = (Button) findViewById(R.id.button_green);
        button_green.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                button_green.setBackgroundColor(Color.GREEN);
           }

        });

        button_yellow = (Button) findViewById(R.id.button_yellow);
        button_yellow.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                button_yellow.setBackgroundColor(Color.YELLOW);
            }

        });


        button_red1 = (Button) findViewById(R.id.button_red1);
        button_red1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                button_red1.setBackgroundColor(Color.RED);
            }

        });


        button_blue = (Button) findViewById(R.id.button_blue);
        button_blue.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                button_blue.setBackgroundColor(Color.BLUE);
            }

        });


        button_black = (Button) findViewById(R.id.button_black);
        button_black.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                button_black.setBackgroundColor(Color.BLACK);
            }

        });

    }
}

