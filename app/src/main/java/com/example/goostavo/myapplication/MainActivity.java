package com.example.goostavo.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {


    Button button_1;
    Button button2;
    TextView text1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //CHANGE TITLE ACTIVITY
        getSupportActionBar().setTitle("Apple");

        //CHANGE ICON ACTIVITY
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.apple);

        button_1 = (Button) findViewById(R.id.button_1);
        button_1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (button_1.getText() == "Hello") {
                    button_1.setText("World");
                }else{
                    button_1.setText("Hello");
                }

            }
        });

        text1 = (TextView) findViewById(R.id.text1);


        button2 = (Button) findViewById(R.id.button2);
        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (text1.getText() == "ONE") {
                    text1.setText("TWO");
                }else{
                    text1.setText("ONE");
                }

            }


        });

    }
}
